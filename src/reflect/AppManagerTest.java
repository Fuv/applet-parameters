package reflect;

public class AppManagerTest {
	private static final PaymentApplet PAYMENT_APPLET = new PaymentApplet();
	private static final SaleApplet SALE_APPLET = new SaleApplet();

	public static void main(String[] args){
		PAYMENT_APPLET.start(new Event<>(new PaymentAppletParameters()));
		SALE_APPLET.start(new Event<>(new SaleAppletParameters()));
	}
}
