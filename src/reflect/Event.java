package reflect;

public class Event<T extends AppletParameters> {
	public Event(T param){
		this.param = param;
	}

	private T param;

	public T getParameters(){
		return param;
	}

}
