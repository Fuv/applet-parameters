package reflect;

public interface ParameterizableApplet<T extends AppletParameters> {
	void start(Event<T> event);
}
